import { EhState, expect } from './test-kit';

describe('EhState', function() {
  const sLanguageTemplate = () => ({ language: 'en', phase: 0 });

  it('Initial State', function(done) {
    const sLanguage = EhState.fromInitialState(sLanguageTemplate());
    const { unregister } = sLanguage.register(langState => {
      const { language } = langState;
      expect(language).to.be.equal('en');
    });
    unregister();
    done();
  });

  it('Mutation', async function() {
    const sLanguage = EhState.fromInitialState(sLanguageTemplate());
    const { unregister } = sLanguage.register(langState => {
      const { test, language, phase } = langState;
      if (phase === 1) {
        expect(test).to.be.equal(true);
        expect(language).to.be.equal('en');
      }
    });

    await sLanguage.fire({ phase: 1, test: true });
    unregister();
  });

  it('Modification', async function() {
    const sLanguage = EhState.fromInitialState(sLanguageTemplate());
    const { unregister } = sLanguage.register(langState => {
      const { language, phase } = langState;
      if (phase === 2) {
        expect(language).to.be.equal('he');
      }
    });

    await sLanguage.fire({ language: 'he', phase: 2 });
    unregister();
  });

  it('Registration After Modification', async function() {
    const sLanguage = EhState.fromInitialState(sLanguageTemplate());
    const { unregister: unregister1 } = sLanguage.register(() => {});

    await sLanguage.fire({ phase: 3, language: 'he' });

    const { unregister: unregister2 } = sLanguage.register(langState => {
      const { language, phase } = langState;
      if (phase === 3) {
        expect(language).to.be.equal('he');
      }
    });

    unregister1();
    unregister2();
  });

  it('Key Change', async function() {
    const sLanguage = EhState.fromInitialState({ ...sLanguageTemplate(), dontUpdateMe: true });
    let calls = 0;
    let keyChangeCalls = 0;
    expect(sLanguage.keyChangeHandlersInUse).to.be.false;
    const { unregister } = sLanguage.registerKeyChange('language', ({ language, dontUpdateMe }, newObj) => {
      keyChangeCalls++;
      expect(newObj.language).to.be.equal(language);
      expect(newObj.dontUpdateMe).to.be.undefined;
      expect(dontUpdateMe).to.be.not.undefined;
      expect(calls).to.be.equal(2);
      expect(language).to.be.equal('he');
    });

    expect(sLanguage.keyChangeHandlersInUse).to.be.true;

    const { unregister: unregister2 } = sLanguage.register(({ language, phase }) => {
      calls++;
      console.log('key change', { calls, keyChangeCalls, language, phase });
    });

    await sLanguage.fire({ phase: 1 });
    await sLanguage.fire({ phase: 2, language: 'he' });
    unregister();
    expect(sLanguage.keyChangeHandlersInUse).to.be.false;
    await sLanguage.fire({ phase: 3, language: 'en' });
    expect(calls).to.be.equal(4);
    expect(keyChangeCalls).to.be.equal(1);

    unregister2();
  });

  it('Delete key', async function() {
    const sLanguage = EhState.fromInitialState({ ...sLanguageTemplate(), dontUpdateMe: true });

    let calls = 0;
    sLanguage.register(data => {
      calls++;

      if (calls === 1) {
        expect(data.language).to.exist;
        expect(data.phase).to.exist;
      } else if (calls === 2) {
        expect(data.language).to.not.exist;
        expect(data.phase).to.exist;
      } else expect(false).to.be.true;
    });

    await sLanguage.deleteStateKey('language');
  });

  it('Fire state (without merge)', async function() {
    const sLanguage = EhState.fromInitialState({ ...sLanguageTemplate(), dontUpdateMe: true });

    sLanguage.register(data => {
      expect(data.phase).to.exist;
      if (data.phase === 0) {
        expect(data.language).to.exist;
      } else if (data.phase === 1) {
        expect(data.language).to.not.exist;
      } else expect(false).to.be.true;
    });

    await sLanguage.fireState({ phase: 1 });
  });
});
