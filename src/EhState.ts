import { EventHub } from '.';
import { EhEvent, EhEventHandler, makeName } from './EhEvent';
import { eh } from './EventHub';

export type EhStateKeyChangeHandler<T> = (state: T, newData: Partial<T>) => Promise<T>;

export class EhState<T extends object> extends EhEvent<T> {
  /**
   *
   * @param initialState An instance (partial or complete) of this object will be expected when state-event is fired and handled
   * @param name A unique name for the event. Defaults to a random unique name
   * @param eventHub A custom EventHub for this EhEvent to use. Defaults to the global event handler (recommended)
   */
  public static fromInitialState<ST extends object>(
    initialState: ST,
    name: string = makeName(initialState.constructor.name),
    eventHub: EventHub = eh,
  ) {
    return new EhState<ST>(initialState, name, eventHub);
  }

  /**
   * USE THIS FROM EhEvent
   */
  public static fromInstance<ST extends object>(instance: ST, name: string, eventHub: EventHub) {
    throw new Error('This function is for EhEvent');
    return new EhEvent<ST>('');
  }

  /**
   * USE THIS FROM EhEvent
   */
  public static fromClass<ST extends object>(theclass: new () => ST, name: string, eventHub: EventHub) {
    throw new Error('This function is for EhEvent');
    return new EhEvent<ST>('');
  }

  private stateData: T;
  private keyChangeHandlers: Map<keyof T, Array<EhStateKeyChangeHandler<T>>>;
  private keyChangeHandlersInUse = false;

  /**
   *
   * @param initialState The initial state of this EhState. An instance (partial or complete) of this object will be expected when state-event is fired and handled
   * @param name A unique name for the event. Defaults to a random unique name
   * @param eventHub A custom EventHub for this EhEvent to use. Defaults to the global event handler (recommended)
   */
  constructor(initialState: T, name: string, eventHub: EventHub = eh) {
    super(name, eventHub);
    this.stateData = initialState;
    this.keyChangeHandlers = new Map();
  }

  /**
   *
   * @param data Data of type T that will be merged into the saved state of this EhState (using Object.assign). Data will be sent to all handlers of this EhState
   * @param eventHub A custom EventHub to to fire data with. Defaults to initially registered EventHub (recommended)
   */
  public fire(data: Partial<T>, eventHub: EventHub = this.eventHub) {
    return this.fireState(data, true, eventHub);
  }

  public fireState(data: Partial<T>, merge: boolean = false, eventHub: EventHub = this.eventHub) {
    const keyChangeInvokes: Array<keyof T> = [];
    if (this.keyChangeHandlersInUse) {
      Object.keys(data).forEach(keystr => {
        const key = keystr as keyof T;
        if (false === this.keyChangeHandlers.has(key)) return;

        const dataVal = data[key];
        const stateVal = this.stateData[key];

        if (dataVal !== stateVal) {
          keyChangeInvokes.push(key);
        }
      });
    }
    if (merge) Object.assign(this.stateData, data);
    else this.stateData = data as T;

    keyChangeInvokes.forEach(key => this.keyChangeHandlers.get(key)?.forEach(handler => handler(this.stateData, data)));
    return super.fire(this.stateData, eventHub);
  }

  /**
   * Register a handler that will be invoked instantly with the current state, as well as each time the state is changed
   * @param handler function that handles the state change. The function expects a T object
   * @param eventHub A custom EventHub to register this handler with. Defaults to initially registered EventHub (recommended)
   */
  public register(handler: EhEventHandler<T>, eventHub: EventHub = this.eventHub) {
    const retval = super.register(handler, eventHub);
    handler(this.stateData);
    return retval;
  }

  /**
   * Register a handler that will be invoked only when the value of "key" is changed
   * @param key When the value of this key changes, the handler will be invoked
   * @param handler Handler to be invoke when the value of key changes
   * @returns
   */
  public registerKeyChange(key: keyof T, handler: EhStateKeyChangeHandler<T>) {
    this.keyChangeHandlersInUse = true;
    if (false === this.keyChangeHandlers.has(key)) {
      this.keyChangeHandlers.set(key, [handler]);
    } else this.keyChangeHandlers.get(key)?.push(handler);

    return {
      handlers: this.keyChangeHandlers.get(key),
      unregister: () => {
        const newList = this.keyChangeHandlers.get(key)?.filter(h => h !== handler);
        if (newList && newList.length > 0) this.keyChangeHandlers.set(key, newList);
        else {
          this.keyChangeHandlers.delete(key);
          if (this.keyChangeHandlers.size === 0) {
            this.keyChangeHandlersInUse = false;
          }
        }
      },
    };
  }

  public deleteStateKey(key: keyof T) {
    const { [key]: v, ...newState } = this.stateData;
    return this.fireState(newState as T);
  }

  /**
   * Returns a DIRECT REFERENCE to the current state object. DO NOT CHANGE THE RETURNED VALUE DIRECTLY
   */
  public get state() {
    return this.stateData;
  }
}
